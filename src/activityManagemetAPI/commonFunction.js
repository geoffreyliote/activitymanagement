export function filterDatas(datas, options) {
    let filterDatas = []
    datas.forEach(data => {
        let filterData = data
        if (options && options.where) {
            let where = options.where
            Object.keys(where).forEach(key => {
                if (data[key] != where[key]) {
                    filterData = null
                }
            })
        }
        if (filterData) filterDatas.push(filterData)
    })
    return filterDatas
}
