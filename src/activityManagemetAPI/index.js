import Vue from "vue";
import axios from 'axios'
import jwt from 'jsonwebtoken'

let instance

export const useActivityManagemetAPI = () => {
    if (instance) return instance;
    instance = new Vue({
        data() {
            return {
                url: 'http://13.80.146.1/api/',
                privateKey: "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk"
            };
        },
        methods: {
            async getOne(tableName, option) {
                const data = await axios.get(this.url + tableName + '/' + option)
                return data ? data : null
            },
            async getAll(tableName) {
                const data = await axios.get(this.url + tableName)
                return data ? data : null
            },
            async getLogin(username, password) {
                const token = await jwt.sign({ Username: username , Password: password}, this.privateKey, { algorithm: 'HS256' })
                const request = await axios.get(this.url + 'UserLogin/' + token )
                return request
            },
            async saveReport(idUser, idActivity, idState, comment) {
                const request = await axios.post(this.url + 'Report/add', 
                {
                    UserId : idUser,
                    ActivityId : idActivity,
                    StateId : idState,
                    Comment : comment
                })
                return request
            },
        },
    });
    return instance;
};



export const ActivityManagemetAPI = {
    install(Vue, options) {
        Vue.prototype.$amAPI = useActivityManagemetAPI(options);
    }
};
